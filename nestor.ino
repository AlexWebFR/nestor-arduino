/*
  Nestor code
*/

// include RC522 library from 
#include <SPI.h>
#include <MFRC522.h>

// Pin layout for RC522 module:
// SS=SDA, 53 for Mega, SCK 52, MOSI 51, MISO 50
// Arduino Uno: RST: 9, SS (SDA): 10, MOSI: 11, MISO: 12, SCK: 13
// Arduino Nano: RST: D9, SS (SDA): D10, MOSI: D11, MISO: D12, SCK: D13
#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance.

// Declare our input/outputs pins
int pushButton = 2;
int relayPin = 6;
int rfidPin = 4;

/**
 * Valid UID's:
 * Card UID: 
 * 7A CC 98 15
 * 0A 08 64 15
 * 4A 64 64 15
 * 0A 86 76 15
 * D9 16 CE 7E
 */

// Declare variables
byte readCard[4];
char* myTags[100] = {};
int tagsCount = 0;
String tagID = "";
boolean successRead = false;
boolean correctTag = false;
bool openSignal = false;

void setup() {
  myTags[0] = "7ACC9815";   // not the good syntax
  myTags[1] = "0A086415";
  myTags[2] = "4A646415";
  myTags[3] = "0A867615";
  myTags[4] = "D916CE7E";
  
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  
  // declare Inputs
  pinMode(pushButton, INPUT);
  pinMode(rfidPin, INPUT);
  pinMode(relayPin, OUTPUT);

  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card
  Serial.println("Scan PICC to see UID and type...");
}

void loop() {
  openSignal = false;
  
  // Read the button state:
  int buttonState = readButton(pushButton);
  
  // Read the RFID state:
  int rfidState = readBadge(rfidPin);

  if(buttonState != 0 || rfidState != 0) {
    openSignal = true;
    sendOpenSignal();
  }
  
  // print out the signal:
  //Serial.println(openSignal);
  
  delay(1);        // delay in between reads for stability
}

// Read the button state
int readButton(int pushButton) {
  int buttonState = digitalRead(pushButton);
  return buttonState;
}

// Read the button state
int readBadge(int rfidPin) {
  // temp: just connect a pin to +5V
  // int rfidState = digitalRead(rfidPin);
  // return rfidState;

  // Switch to THIS when received RFID reader...
  /**
   * Go to https://arduino.stackexchange.com/questions/59855/emulating-a-generic-mifare-usb-peripheral-device-with-rfid-rc522
   * and https://howtomechatronics.com/tutorials/arduino/rfid-works-make-arduino-based-rfid-door-lock/
   * Library: https://github.com/miguelbalboa/rfid
   * Experimentboy code: https://pastebin.com/gY2dM05J
   * 
   **/

  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return 0;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return 0;
  }

  // Affichage des informations de la carte RFID
  mfrc522.PICC_DumpToSerial(&(mfrc522.uid));

}

uint8_t getID() {
  // Getting ready for Reading PICCs
  if ( ! mfrc522.PICC_IsNewCardPresent()) { //If a new PICC placed to RFID reader continue
    return 0;
  }
  if ( ! mfrc522.PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return 0;
  }
  tagID = "";
  for ( uint8_t i = 0; i < 4; i++) {  // The MIFARE PICCs that we use have 4 byte UID
    readCard[i] = mfrc522.uid.uidByte[i];
    tagID.concat(String(mfrc522.uid.uidByte[i], HEX)); // Adds the 4 bytes in a single String variable
  }
  tagID.toUpperCase();
  mfrc522.PICC_HaltA(); // Stop reading
  return 1;
}

// Send opening signal to the door
void sendOpenSignal() {
  // temporary only turn a LED on
  digitalWrite(relayPin, HIGH);
  delay(500);
  digitalWrite(relayPin, LOW);
}
