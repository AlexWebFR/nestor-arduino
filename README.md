# Nestor

Nestor est un portier intelligent.
Il commande l'ouverture d'une gâche électrique 
et potentiellement aussi un portail de garage.

Les commandes d'ouvertures peuvent de faire de plusieurs manières:
- via un écran vidéo dans la maison (système pré-existant) auquel cas Nestor n'a rien à faire
- via un bouton présent sur le mur d'enceinte de la maison, inaccessible depuis la rue
- via un lecteur de badge RFID RC-522

La gâche électrique attend une impulsion 12 volts pour s'ouvrir. Il s'agit d'une gâche à mémoire,
donc une impulsion d'une seconde est amplement suffisante pour pouvoir ouvrir la porte, car la gâche
électrique ne repasse en position fermée qu'après une ouverture et une fermeture de la porte.

Pour envoyer les 12v dans la gâche, j'utilise un module relais qui sera donc contrôlé par l'arduino.